package com.Resources;

import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.TEAF.Hooks.Hooks;
import com.TEAF.framework.Utilities;
import com.google.common.io.Files;

public class Utility extends com.TEAF.framework.Utilities {

	public static void auto_generateEmail(String mailId, String ccMail) {

		// Create object of Property file
		Properties props = new Properties();
		props.put("mail.smtp.host", System.getProperty("email.smtp.server"));
		props.put("mail.smtp.socketFactory.port", System.getProperty("email.smtp.socketPortNumber"));
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", System.getProperty("email.smtp.portNumber"));
		props.put("mail.smtp.starttls.enable", "true");

		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(System.getProperty("email.user.emailId"),
						System.getProperty("email.user.password"));
			}
		});

		try {

			// Create object of MimeMessage class
			Message message = new MimeMessage(session);

			// Set the from address
			message.setFrom(new InternetAddress(System.getProperty("email.user.emailAddress")));
			// String mailToId2 = "chandan.navara@royalcyber.com";
			// String mailToId3 = "swathin@royalcyber.com";
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse(mailToId2));
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMail));
			// Set the recipient
			// address
			String lDate = LocalDate.now().toString();
			String lTime = LocalTime.now().toString();
			// Add the subject link

			Collection<String> values = Hooks.scenarioStatus.values();
			String flag;
			int failCount = 0;
			int passCount = 0;
			int skipCount = 0;
			for (String x : values) {
				if (x.equalsIgnoreCase("Failed")) {
					failCount++;
				} else if (x.equalsIgnoreCase("Passed")) {
					passCount++;
				} else {
					skipCount++;
				}
			}
			if (values.contains("FAILED") || values.contains("SKIPPED")) {
				flag = "Build Failed";
			} else {
				flag = "Build Passed";
			}
			String subject = "RC TEAF Execution Report - " + ":" + " on " + lDate + ": " + lTime + " :" + flag;
			message.setSubject(subject);
			MimeMultipart multipart = new MimeMultipart("related");

			// Create object to add multimedia type content
			BodyPart messageBodyPart1 = new MimeBodyPart();

			StringBuffer ScenarioTable = new StringBuffer();
			Map<String, String> mp = new TreeMap<String, String>();
			mp.putAll(Hooks.scenarioStatus);
			Set<Entry<String, String>> entrySet = mp.entrySet();
			
			for (Entry<String, String> entry : entrySet) {
				
				if (entry.getValue().equalsIgnoreCase("passed")) {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
					+ "</TD><TD bgcolor= '#419c4d' >" + entry.getValue() + "</TD> " + "</TR>");
				}else {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
					+ "</TD><TD bgcolor= '#d63a29' >" + entry.getValue() + "</TD> " + "</TR>");
				}
				

			}
			// Set the body of email
			String htmlText = "<img src=\"cid:image\"><BR><H1>Script Execution Summary | Email Report </H1><BR>"
					+"<BR><H1>TEST URL : "+System.getProperty("test.appUrl")+"</H1><BR>"
					+ "<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"
					+ "<TR> <TH bgcolor = '#a8a7a7' COLSPAN='4'><H2>Test Execution Summary</H2></TH></TR><TR><TH>Total Test Cases: </TH><TH>Total TCs Passed: </TH><TH>Total TCs Failed: </TH><TH>Total TCs Skipped: </TH>"
					+ "</TR>" + "<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + values.size()
					+ "</TD><TD bgcolor= '#419c4d' >" + passCount + "</TD><TD bgcolor= '#d63a29' >" + failCount
					+ "</TD><TD bgcolor= '#6bbbc7' >" + skipCount + "</TD>" + "</TR>"
					+ "</TABLE><H4>Please find attached a detailed Test Automation execution report using RC TEAF with this email</H4> "+"\n" 
					+ "\n" 
					+"<H4>Test Scenarios executed:</H4>"+
					"<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"+"<TR ALIGN='CENTER' bgcolor ='#3767B6'><TD color: 'white'> Scenario Name </TD><TD bgcolor ='#3767B6' color: 'white'>Result</TD></TR>"+
					ScenarioTable+"</TABLE>"+"\n"+"\n"+
					
					"Thanks, " + "\n" +
					"\n"
					+ "RC QA";
			messageBodyPart1.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart1);

			// second part (the image)
			messageBodyPart1 = new MimeBodyPart();
			DataSource fds = new FileDataSource(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\teaf.jpeg");

			messageBodyPart1.setDataHandler(new DataHandler(fds));
			messageBodyPart1.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart1);

			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			MimeBodyPart messageBodyPart3 = new MimeBodyPart();

			// Mention the file which you want to send
			String fEN = System.getProperty("user.dir") + "/TestExecution_ExtentReports.zip";
			String fUI = System.getProperty("user.dir") + "/TestExecution_UIReports.zip";

			multipart.addBodyPart(messageBodyPart1);

			if (java.nio.file.Files.exists(Paths.get(fEN), LinkOption.NOFOLLOW_LINKS)) {
				DataSource src = new FileDataSource(fEN);
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(src));
				// set the file
				messageBodyPart2.setFileName(fEN);
				multipart.addBodyPart(messageBodyPart2);
			}

			if (java.nio.file.Files.exists(Paths.get(fUI), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source1 = new FileDataSource(fUI);

				// set the handler
				messageBodyPart3.setDataHandler(new DataHandler(source1));

				// set the file
				messageBodyPart3.setFileName(fUI);
				multipart.addBodyPart(messageBodyPart3);

			}
			message.setContent(multipart);

			// finally send the email
			Transport.send(message);

			System.out.println("\n =====Reports Sent through Email from "
					+ System.getProperty("email.user.emailAddress") + "=====");

		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
			Utilities.deleteZipFiles("TestExecution_ExtentReports");
			Utilities.deleteZipFiles("TestExecution_UIReports");
		}
	}
	
	public static void main(String[] args) {
		
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < 10; i++) {
			s.append("<TR>"+i+"</TR>");
		}
		System.out.println(s);
	}

}
