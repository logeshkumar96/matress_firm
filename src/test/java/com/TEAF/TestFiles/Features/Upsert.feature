@Test
Feature: Testing HTTP methods using cucumber BDD 

@test11
Scenario: Email config
Given URL 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/emailconfig'
And Content type 'application/json'
And Run the EmailConFig
And Request body 'C:\\Users\\User\\Desktop\\new3.txt'
When Method 'Post'
Then Statuscode 200
#Then Print response
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Emailconfiguration-res.json'


@test22
Scenario: Email notification
Given URL 'http://mule-worker-internal-sys-email-dev-v1.us-w2.cloudhub.io:8091/mail/send-email-notification'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\Emailnotification-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Emailnotification-res.json'



#Then I should pick value from file 'C:\\Users\\User\\Desktop\\req&RES\\Emailconfiguration-res.json' and value 'FROM_ADDRESS' and update the file 'C:\\Users\\User\\Desktop\\req&RES\\Emailnotification-req.json'

@test33
Scenario: Insert log details
Given URL 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/insert-log-details'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\Insertlogdetails-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Insertlogdetails-res.json'

@test44
Scenario: Fetch service run
Given URL 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/fetch-service-run'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\Fetchservicerun-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Fetchservicerun-res.json'

@test55
Scenario: Fetch Elastic Search Index
Given URL 'http://mule-worker-internal-sys-es-dev-v1.us-w2.cloudhub.io:8091/elasticsearch/fetch-all-elasticsearch-index'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\Elasticsearchindex-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Elasticsearchindex-res.json'

@test66
Scenario: Salesforce WarehouseList
Given URL 'http://mule-worker-internal-sys-salesforce-dev-v1.us-w2.cloudhub.io:8091/salesforce/warehouseList'
And Content type 'application/json'
#And Request body 'C:\\Users\\User\\Desktop\\req&RES\\Elasticsearchindex-req.json'
When Method 'Get'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\Warehouselist-res.json'

@test77
Scenario: sdf customer upsert
Given URL 'http://mule-worker-internal-sys-salesforce-dev-v1.us-w2.cloudhub.io:8091/salesforce/sdfcustomerupsert'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\SFDCupsert-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\SFDCupsert-res.json'

@test88
Scenario: timestamp
Given URL 'http://mule-worker-internal-sys-sqlserver-dev-v1.us-w2.cloudhub.io:8091/database/timestamp'
And Content type 'application/json'
And Request body 'C:\\Users\\User\\Desktop\\req&RES\\timestamp-req.json'
When Method 'Post'
Then Statuscode 200
Then Print response to file 'C:\\Users\\User\\Desktop\\req&RES\\timestamp-res.json'



