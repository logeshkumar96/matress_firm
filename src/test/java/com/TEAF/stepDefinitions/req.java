package com.TEAF.stepDefinitions;

public class req {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	private String sortingFieldName;
	private String lastProcessedKey;
	private String timest;
	private String index;
	private int esDeltaQuerySize;
	private String lastRunDate;

	public String getSortingFieldName() {
	return sortingFieldName;
	}

	public void setSortingFieldName(String sortingFieldName) {
	this.sortingFieldName = sortingFieldName;
	}

	public String getLastProcessedKey() {
	return lastProcessedKey;
	}

	public void setLastProcessedKey(String lastProcessedKey) {
	this.lastProcessedKey = lastProcessedKey;
	}

	public String getTimest() {
	return timest;
	}

	public void setTimest(String timest) {
	this.timest = timest;
	}

	public String getIndex() {
	return index;
	}

	public void setIndex(String index) {
	this.index = index;
	}

	public int getEsDeltaQuerySize() {
	return esDeltaQuerySize;
	}

	public int setEsDeltaQuerySize(int esDeltaQuerySize) {
	return this.esDeltaQuerySize = esDeltaQuerySize;
	}

	public String getLastRunDate() {
	return lastRunDate;
	}

	public void setLastRunDate(String lastRunDate) {
	this.lastRunDate = lastRunDate;
	}

	@Override
	public String toString() {
		return "Data [sortingFieldName=" + sortingFieldName + ", lastProcessedKey=" + lastProcessedKey + ", timest="
				+ timest + ", index=" + index + ", esDeltaQuerySize=" + esDeltaQuerySize + ", lastRunDate="
				+ lastRunDate + "]";
	}

	}
	


