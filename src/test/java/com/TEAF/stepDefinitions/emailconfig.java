package com.TEAF.stepDefinitions;

public class emailconfig {

//	public static void main(String[] args) {
		

//		public class Data {

		private String uuid;
		private String applicationName;
		private String flowName;
		private boolean jobRunEmailDistroFlag;
		private String emailSubject;
		private String emailBodyContent;

		public String getUuid() {
		return uuid;
		}

		public void setUuid(String uuid) {
		this.uuid = uuid;
		}

		public String getApplicationName() {
		return applicationName;
		}

		public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
		}

		public String getFlowName() {
		return flowName;
		}

		public void setFlowName(String flowName) {
		this.flowName = flowName;
		}

		public boolean isJobRunEmailDistroFlag() {
		return jobRunEmailDistroFlag;
		}

		public void setJobRunEmailDistroFlag(boolean jobRunEmailDistroFlag) {
		this.jobRunEmailDistroFlag = jobRunEmailDistroFlag;
		}

		public String getEmailSubject() {
		return emailSubject;
		}

		public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
		}

		public String getEmailBodyContent() {
		return emailBodyContent;
		}

		public void setEmailBodyContent(String emailBodyContent) {
		this.emailBodyContent = emailBodyContent;
		}

		@Override
		public String toString() {
			return "emailconfig [uuid=" + uuid + ", applicationName=" + applicationName + ", flowName=" + flowName
					+ ", jobRunEmailDistroFlag=" + jobRunEmailDistroFlag + ", emailSubject=" + emailSubject
					+ ", emailBodyContent=" + emailBodyContent + "]";
		}

		
		}




