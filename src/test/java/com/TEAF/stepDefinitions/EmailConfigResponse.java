package com.TEAF.stepDefinitions;

public class EmailConfigResponse {

	

		private String INTEGRATION_NAME;
		private String APPLICATION_NAME;
		private String JOB_RUN_EMAIL_DISTRO;
		private String FLOW_NAME;
		private String JOB_FAILURE_EMAIL_DISTRO;
		private String FROM_ADDRESS;

		public String getINTEGRATION_NAME() {
		return INTEGRATION_NAME;
		}

		public void setINTEGRATION_NAME(String INTEGRATION_NAME) {
		this.INTEGRATION_NAME = INTEGRATION_NAME;
		}

		public String getAPPLICATION_NAME() {
		return APPLICATION_NAME;
		}

		public void setAPPLICATION_NAME(String APPLICATION_NAME) {
		this.APPLICATION_NAME = APPLICATION_NAME;
		}

		public String getJOB_RUN_EMAIL_DISTRO() {
		return JOB_RUN_EMAIL_DISTRO;
		}

		public void setJOB_RUN_EMAIL_DISTRO(String JOB_RUN_EMAIL_DISTRO) {
		this.JOB_RUN_EMAIL_DISTRO = JOB_RUN_EMAIL_DISTRO;
		}

		public String getFLOW_NAME() {
		return FLOW_NAME;
		}

		public void setFLOW_NAME(String FLOW_NAME) {
		this.FLOW_NAME = FLOW_NAME;
		}

		public String getJOB_FAILURE_EMAIL_DISTRO() {
		return JOB_FAILURE_EMAIL_DISTRO;
		}

		public void setJOB_FAILURE_EMAIL_DISTRO(String JOB_FAILURE_EMAIL_DISTRO) {
		this.JOB_FAILURE_EMAIL_DISTRO = JOB_FAILURE_EMAIL_DISTRO;
		}

		public String getFROM_ADDRESS() {
		return FROM_ADDRESS;
		}

		public void setFROM_ADDRESS(String FROM_ADDRESS) {
		this.FROM_ADDRESS = FROM_ADDRESS;
		}
	
		}