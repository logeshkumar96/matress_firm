package com.TEAF.stepDefinitions;

import static io.restassured.RestAssured.given;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class MainClass {
	Response response;
	ValidatableResponse VResponse;
	static RequestSpecification spec;
	static String key[] = new String[20];
	static String value[] = new String[20];

	@Test
	public Response Get(String path) {
		response = given().when().get(path);
		return response;
	}

	@Test
	public Response Post(String url, String path) {
		File f = new File(path);

		response = given().when().header("Content-Type", "application/json").body(f).post(url);
		return response;
	}

	@Test
	public Response Put(String url, String path) {
		File f = new File(path);
		response = given().when().body(f).patch(url);
		return response;
	}

	@Test
	public Response Delete(String url) {
		response = given().when().delete(url);
		return response;
	}

	@Test
	public Response SOAP(String url, String file) throws Throwable {
		FileInputStream fileinputstream = new FileInputStream(file);
		RestAssured.baseURI = url;
		Response response = given().header("Content-Type", "text/xml").and()
				.body(IOUtils.toString(fileinputstream, "UTF-8")).when().post("");
		return response;
	}

	public Response Post(String path, JSONObject requestParameters) {

		response = given().when().header("Content-Type", "application/json").body(requestParameters).post(path);
		return response;
	}

	public Response Post(String path, RequestSpecification spec2) {
		response = spec2.post(path);
		return response;
	}

	public Response Get(String path, RequestSpecification spec2) {
		response = spec2.get(path);
		return response;
	}

	public Response Put(String path, RequestSpecification spec2) {
		response = spec2.put(path);
		return response;
	}

	public Response Delete(String path, RequestSpecification spec2) {
		response = spec2.delete(path);
		return response;
	}

	public Response Post(String string) {
		response = spec.post(string);
		return response;
	}

	public Response Put(String string) {
		response = spec.put(string);
		return response;
	}
	
	
	public Response Delete(String path, String input)
	{
		File file = new File(input);
		response = spec.body(file).when().post(path);
		return response;
	}
}