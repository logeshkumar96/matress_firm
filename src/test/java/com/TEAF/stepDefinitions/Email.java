
package com.TEAF.stepDefinitions;
//package RCTeAF.Temp;

import com.google.gson.Gson;


public class Email{

		private String uuid;
		private String applicationName;
		private String flowName;
		private boolean jobRunEmailDistroFlag;
		private String emailSubject;
		private String emailBodyContent;
		private String INTEGRATION_NAME;
		private String JOB_RUN_EMAIL_DISTRO;
		private String JOB_FAILURE_EMAIL_DISTRO;
		private String FROM_ADDRESS;
		

		public String getUuid() {
		return uuid;
		}

		public void setUuid(String uuid) {
		this.uuid = uuid;
		}

		public String getApplicationName() {
		return applicationName;
		}

		public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
		}

		public String getFlowName() {
		return flowName;
		}

		public void setFlowName(String flowName) {
		this.flowName = flowName;
		}

		public boolean isJobRunEmailDistroFlag() {
		return jobRunEmailDistroFlag;
		}

		public void setJobRunEmailDistroFlag(boolean jobRunEmailDistroFlag) {
		this.jobRunEmailDistroFlag = jobRunEmailDistroFlag;
		}

		public String getEmailSubject() {
		return emailSubject;
		}

		public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
		}

		public String getEmailBodyContent() {
		return emailBodyContent;
		}

		public void setEmailBodyContent(String emailBodyContent) {
		this.emailBodyContent = emailBodyContent;
		}

		public String getiNTEGRATIONNAME() {
		return INTEGRATION_NAME;
		}

		public void setiNTEGRATIONNAME(String INTEGRATIONNAME) {
		this.INTEGRATION_NAME = INTEGRATIONNAME;
		}

		public String getjOBRUNEMAILDISTRO() {
		return JOB_RUN_EMAIL_DISTRO;
		}

		public void setjOBRUNEMAILDISTRO(String JOBRUNEMAILDISTRO) {
		this.JOB_RUN_EMAIL_DISTRO = JOBRUNEMAILDISTRO;
		}

		public String getjOBFAILUREEMAILDISTRO() {
		return JOB_FAILURE_EMAIL_DISTRO;
		}

		public void setjOBFAILUREEMAILDISTRO(String JOBFAILUREEMAILDISTRO) {
		this.JOB_FAILURE_EMAIL_DISTRO = JOBFAILUREEMAILDISTRO;
		}

		public String getfROMADDRESS() {
		return FROM_ADDRESS;
		}

		public void setfROMADDRESS(String FROMADDRESS) {
		this.FROM_ADDRESS = FROMADDRESS;
		}

		@Override
		public String toString() {
			return "Pojo [uuid=" + uuid + ", applicationName=" + applicationName + ", flowName=" + flowName
					+ ", jobRunEmailDistroFlag=" + jobRunEmailDistroFlag + ", emailSubject=" + emailSubject
					+ ", emailBodyContent=" + emailBodyContent + ", INTEGRATIONNAME=" + INTEGRATION_NAME
					+ ", JOBRUNEMAILDISTRO=" + JOB_RUN_EMAIL_DISTRO + ", JOB_FAILURE_EMAIL_DISTRO=" + JOB_FAILURE_EMAIL_DISTRO
					+ ", FROMADDRESS=" + FROM_ADDRESS + "]";
		}

		public static void main(String[] args) {
			Gson  gson = new Gson();
			
			Email email = new Email();
			email.setUuid("123123");
			email.setApplicationName("MuleSoftApp");
			email.setFlowName("TestFlowName");
			email.setJobRunEmailDistroFlag(false);
			email.setEmailSubject("testemailsubject");
			email.setEmailBodyContent("testemailbodycontent");
			email.setiNTEGRATIONNAME("testintegrationName");
			email.setjOBRUNEMAILDISTRO("testemailDistroValue");
			email.setjOBFAILUREEMAILDISTRO("testJobFailureEmailDistro");
			emailtojson etj = new emailtojson();
			etj.Pick();
			email.setfROMADDRESS();
			String json = gson.toJson(email);
			
			//--------Object to JSON-------------------------------------
			System.out.println("Json: "+json);
			//--------------------------------------------
			
			
			//--------JSON to Object-------------------------------------
			email = gson.fromJson(json, Email.class);
			System.out.println(email.getApplicationName());
			
			Email fromaddress = gson.fromJson(json, Email.class);
					System.out.println(email.getfROMADDRESS());
		}
}