package com.TEAF.stepDefinitions;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class reqtojson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		req dt =new req();{
			dt = getObjectData(dt);
			ObjectMapper Obj = new ObjectMapper();
			try { 
				  
		        String jsonStr = Obj.writeValueAsString(dt); 

		        System.out.println(jsonStr); 
		    } 

		    catch (IOException e) { 
		        e.printStackTrace(); 
		    } 
		} 
		}

	private static req getObjectData(req dt) {
		
		dt.setSortingFieldName("Id");
		dt.setLastProcessedKey("1606");
		dt.setTimest("1535597221000");
		dt.setIndex("store");
		dt.setEsDeltaQuerySize(2);
		dt.setLastRunDate("2018/08/29 21:47:01");
		
		return dt;
	}
}