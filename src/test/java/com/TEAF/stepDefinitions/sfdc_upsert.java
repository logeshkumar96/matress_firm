package com.TEAF.stepDefinitions;

import java.util.ArrayList;
import java.util.List;

public class sfdc_upsert {

private String sObjectType;
private String flowType;
private String primarykey;
private List<UpsertPayload> upsertPayload = new ArrayList<UpsertPayload>();

public String getSObjectType() {
return sObjectType;
}

public void setSObjectType(String sObjectType) {
this.sObjectType = sObjectType;
}

public String getFlowType() {
return flowType;
}

public void setFlowType(String flowType) {
this.flowType = flowType;
}

public String getPrimarykey() {
return primarykey;
}

public void setPrimarykey(String primarykey) {
this.primarykey = primarykey;
}

public List<UpsertPayload> getUpsertPayload() {
return upsertPayload;
}

public void setUpsertPayload(List<UpsertPayload> upsertPayload) {
this.upsertPayload = upsertPayload;
}

}