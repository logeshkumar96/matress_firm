package com.TEAF.stepDefinitions;


public class Info {

private String serviceName;
private String callerName;
private String timestamp;
private String lastProcessedkey;

public String getServiceName() {
return serviceName;
}

public void setServiceName(String serviceName) {
this.serviceName = serviceName;
}

public String getCallerName() {
return callerName;
}

public void setCallerName(String callerName) {
this.callerName = callerName;
}

public String getTimestamp() {
return timestamp;
}

public void setTimestamp(String timestamp) {
this.timestamp = timestamp;
}

public String getLastProcessedkey() {
return lastProcessedkey;
}

public void setLastProcessedkey(String lastProcessedkey) {
this.lastProcessedkey = lastProcessedkey;
}

}