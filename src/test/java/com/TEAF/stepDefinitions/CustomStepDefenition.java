package com.TEAF.stepDefinitions;

import static org.junit.Assert.assertEquals;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.hamcrest.Matcher;
import org.hamcrest.SelfDescribing;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
//import com.mongodb.MongoOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.Header;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
//import javafx.util.Pair;

public class CustomStepDefenition {

       MainClass Rest = new MainClass();
       public static RequestSpecification spec = given().when();
       public static Header h;
       public static String Path;
       public static Response GetResponse;
       public static ValidatableResponse Response;
       public static Scenario s;
       static JSONObject RequestParameters = new JSONObject();
       public static String Temp_Variable;
       public static JSONObject jsonobject = new JSONObject();
       public static String sqlresultpath;
       public static String Mongofilepath;

       @After()
       public void after_scenario() {
              System.out.println("\n Next Scenario");
       }

       @Before("@First")
       public void before_scenario1() {
              // System.out.println("\n+++++++++++++++++++Initiate
              // http://localhost:3030/products\n");
       }

       @After("@First")
       public void after_scenario1() {
              // System.out.println("\n+++++++++++++++++Close
              // http://localhost:3030/products\n");
       }

       @Before()
       public void before_scenario(Scenario scenario) {
              this.s = scenario;
              // System.out.println("\nInitiate http://localhost:3030/products\n");
       }

       @Given("URL {string}")
       public String URL(String url) {

              Path = url;
              return Path;

       }

       @When("Method {string}")

       public void Method(String Method_Name) {

              switch (Method_Name) {
              case "Get":
                     GetResponse = Rest.Get(Path, spec);
                     break;
              case "Delete":
                     GetResponse = Rest.Delete(Path, spec);
                     break;
              case "Post":
                     GetResponse = Rest.Post(Path, spec);
                     break;
              case "Put":
                     GetResponse = Rest.Post(Path, spec);
                     break;
              }
       }

       @Then("Statuscode {int}")
       public void statuscode(int Expected) {

              int Actual = GetResponse.andReturn().statusCode();
              assertEquals("Status code Validation", Expected, Actual);
       }

       @Then("Match JSONPath {string} contains {string}")
       public void match_JSONPath_contains(String path, String Expected) {

              String Actual = GetResponse.getBody().jsonPath().get(path).toString();
              assertEquals(" Json Path Value Check ", Expected, Actual);
       }

       @Then("Match header {string} contains {string}")
       public void match_header_contains(String string, String Expected) {

              String Actual = GetResponse.getHeader(string);
              if (Objects.equals(Actual, Expected))
                     assertEquals("Header Matching", Expected, Actual);

       }

       @Then("Print responsetime")
       public void print_responsetime() {
              long temp = GetResponse.getTime();
              System.out.println("Response Time" + temp + "ms");
       }

       @Then("Print response")
       public void print_response() {
              GetResponse.getBody().prettyPrint();
       }

       @Then("Print response to file {string}")
       public void print_response_to_file(String path) throws ClassNotFoundException, IOException, ParseException {
              String Response = GetResponse.getBody().prettyPrint();
              File f = new File(path);
              boolean Exists = f.exists();
              if (Exists) {
                     System.out.println("Printing response to File");
                     this.write(f, Response);
              } else {
                     f.createNewFile();
                     System.out.println("New file created");
                     this.write(f, Response);
              }

       }

       private void write(File f, String Response) {
              try {
                     FileOutputStream f1 = new FileOutputStream(f);
                    OutputStreamWriter o = new OutputStreamWriter(f1);
                     o.write(Response);
//                     writeObject(Response);
                     System.out.println(Response);
                     o.close();
                     f1.close();
              } catch (FileNotFoundException e) {
                     System.out.println("File not found");
              } catch (IOException e) {
                     System.out.println("Error initializing stream");
              }
       }

       @And("Match XMLPath {string} contains {string}")
       public void XMLPath(String xmlpath, String Expected) {

              XmlPath xml = new XmlPath(GetResponse.asString());
              String Actual = xml.getString(xmlpath);
              assertEquals("Xml Path String check", Actual, Expected);

       }

       @SuppressWarnings("unchecked")
       @Given("parameter JSON {string} with value {string}")
       public JSONObject parameter_JSON_with_value(String key, String value) {
              RequestParameters.put(key, value);
              System.out.println("\n" + RequestParameters);
              return RequestParameters;
       }

       @SuppressWarnings("unchecked")
       @Given("parameter JSON {string} with value {double}")
       public JSONObject parameter_with_value(String key, Double value) {
              RequestParameters.put(key, value);
              System.out.println("\n" + RequestParameters.toJSONString());
              return RequestParameters;
       }

       @When("Print report")
       public void print_report() {

              s.write(GetResponse.getBody().prettyPrint());

       }

       @When("Method {string} appended with Stored Value")
       public void Method_Appended(String Method_Name) {

              System.out.println("\nMethod name with appended value: " + Method_Name);
              switch (Method_Name) {
              case "Get":
                     GetResponse = Rest.Get(Path + "/" + Temp_Variable, spec);
                     break;
              case "Post":
                     GetResponse = Rest.Post(Path + "/" + Temp_Variable, spec);
                     break;
              case "Put":
                     GetResponse = Rest.Put(Path + "/" + Temp_Variable, spec);
                     break;
              case "Delete":
                     GetResponse = Rest.Delete(Path + "/" + Temp_Variable, spec);
                     break;
              }
       }

       @And("get value of {string} and store in {string}")
       public String getting_recent_value(String JSON_Path, String Local_Temp_Variable) {

              Local_Temp_Variable = GetResponse.getBody().jsonPath().getJsonObject(JSON_Path).toString();
              Temp_Variable = Local_Temp_Variable;
              return Temp_Variable;
       }

       @Given("Query Param name {string} value {string}")
       public void param_name_value(String string, String string2) {
              spec = given().queryParam(string, string2);
       }

       @Given("MySQL Query {string} and store ResultSet in File {string}")
       public void mysql_Query_and_store_ResultSet_in_File(String Query, String string2) throws Throwable {
              Class.forName("com.mysql.cj.jdbc.Driver");
              Connection con = DriverManager.getConnection("jdbc:mysql://Localhost:3306/db1", "root", "root");
              Statement stmt = con.createStatement();
              ResultSet resultSet = stmt.executeQuery(Query);
              sqlresultpath = string2;
              ToJSON(resultSet);
       }
       
       

       @SuppressWarnings("unchecked")
       private static void ToJSON(ResultSet resultSet) throws Throwable {

              while (resultSet.next()) {
                     int total_clmns = resultSet.getMetaData().getColumnCount();
                     for (int i = 1; i <= total_clmns; i++) {

                           String key = resultSet.getMetaData().getColumnLabel(i).toLowerCase();
                           String value = resultSet.getString(i);
                           jsonobject.put(key, value);

                     }
              }
              System.out.println(jsonobject);
              FileWriter file = new FileWriter(sqlresultpath);
              String jsonString = jsonobject.toString();
              file.write(jsonString);
              file.close();
              resultSet.close();
       }

       public void MongoResult(Document dbo) {
              try {

                     List<String> readAllLines = Files.readAllLines(Paths.get(Mongofilepath));
                     StringBuffer sb = new StringBuffer();
                     for (String x : readAllLines) {
                           if (x.contains("_id")) {
                                  sb.append("{");
                                  String[] split = x.split(", ", 2);
                                  sb.append(split[1] + "\n");
                           }
                     }
                     System.out.println(sb);
                     FileWriter fw = new FileWriter(Mongofilepath);
                     fw.write(sb.toString());
                     fw.close();

              } catch (Exception ex) {
                     ex.printStackTrace();

              }
       }

       
//       @Then("^I should print value from file '(.*)' and value '(.*)'$")
//       public static void i_should_pick_and_update(String file, Object ArrayValue) throws Exception {
//           FileReader reader = new FileReader(file);
//           JSONObject jsonObject = (JSONObject) new JSONParser().parse(reader);
//           JSONArray array = (JSONArray) jsonObject.get(ArrayValue);
//           JSONObject temp;
//           long  id ;
//           String first_name,last_name,email;
//           for (int i=0; i<array.size(); i++){
//               temp = (JSONObject) array.get(i);
//               id = (long) temp.get("id");
//               if( id==2){
//                   first_name  = (String) temp.get("first_name");
//                   last_name = (String) temp.get("last_name");
//                   email = (String) temp.get("email");
////                   System.out.println( first_name +" "+last_name);
//                   System.out.println(email);
//               }
//           }
//       }
//       
       
       
//      @Then("^I should print value from file '(.*)' and value '(.*)'$")
//       public static void i_should_pick_and_update1(String file, Object value) throws Exception {
//           FileReader reader = new FileReader(file);
//           FileWriter file1 = new FileWriter("C:\\Users\\User\\Desktop\\test1.json");
//          FileOutputStream newFile;
//           JSONObject obj = new JSONObject();
//           JSONObject jsonObject = (JSONObject) new JSONParser().parse(reader);
//           JSONArray sensors = (JSONArray) jsonObject.get(value);
////           JSONObject temp;
////           long  id ;
//           String first_name,last_name,email,address;
//           for(int i=0; i<sensors.size(); i++) {
////        	   String sensors1 = sensors.toJSONString(); 
////        	   String address = objects.getString (sensors1);
//        	      System.out.println(sensors.get[i]);
//        	    }
//           
//           
//           
////           for (int i=0; i<sensors.size(); i++){
////               temp = (JSONObject) sensors.get(i);
////               id = (long) temp.get("id");
////             
////               if( id==2){
////                   email = (String) temp.get("email");
////                   System.out.println(email);
//                   obj.put("FROM_ADDRESS", movie1);
//                   file1.write(obj.toJSONString());
//                   file1.flush();
//                   file1.close();
//               }
//              
//          }
////       }
//       
       
@Then("^I should pick value from file '(.*)' and value '(.*)' and update the file '(.*)' in the field '(.*)'$")
public static void i_should_pick_and_update(String file, Object value , String file2 , Object value2) throws Exception {
    FileReader reader = new FileReader(file);
    FileWriter file1 = new FileWriter(file2,true);
    FileOutputStream newFile;
//    JSONObject json_obj = new JSONObjectIgnoreDuplicates(file2);
   JSONArray Array = (JSONArray) new JSONParser().parse(reader);
   JSONObject first = (JSONObject) Array.get(0);
   
   JSONObject obj = new JSONObject();
   String objvalue = (String) first.get(value);
   System.out.println(objvalue);
//   JSONArray ja = new JSONArray();
//   ObjectMapper mapper = new ObjectMapper();
   
   obj.put(""+value+"",objvalue);
//  obj.putIfAbsent(""+value+"",objvalue);
   file1.write(obj.toJSONString());
   file1.flush();
   file1.close();
   }
      




////       private static String jsonFile = "/Users/foteini/Desktop/JSON/sensor copy.json";   
//
//       private static String jsonFile = "C:\\Users\\User\\Desktop\\test";   
//
//       public static void main(String[] args) throws FileNotFoundException, IOException, ParseException, Exception {
//
//           FileReader reader = new FileReader(jsonFile);
//
//           JSONObject jsonObject = (JSONObject) new JSONParser().parse(reader);
//
//           JSONArray sensors = (JSONArray) jsonObject.get("Sensor");
//           //take the elements of the json array
//           int id_num=0;
//           for (int i=0; i<sensors.size(); i++){
//               Object item = sensors.get(i);
//               if (((ResultSet) item).getInt("id") == 1){
//                   // process the item
//                   System.out.println(((ResultSet) item).getString("status"));
//               }
//
//            }
//          }
//       
       
       @Given("Find key {string} value {string} from Mongo DB and store ResultSet in File {string}")
       public void find_key_value_from_Mongo_DB_and_store_ResultSet_in_File(String string, String string2, String string3)
                     throws Throwable {
              Document dbo1 = null;
              // MongoDBSingleton dbSingleton = MongoDBSingleton.getInstance();
              String u = "mongodb+srv://Sangavi:Sangavi@cluster0-rx121.mongodb.net/test?retryWrites=true&w=majority";
              MongoClientURI uri = new MongoClientURI(u);
              System.out.println(uri);
              @SuppressWarnings("resource")
              MongoClient mongoClient = new MongoClient(uri);
              MongoDatabase database = mongoClient.getDatabase("db1");
              MongoCollection<Document> collection = database.getCollection("mycollection");
              BasicDBObject query = new BasicDBObject(string, string2);
              MongoCursor<Document> cursor = collection.find(query).iterator();
              try {
                     while (cursor.hasNext()) {
                           dbo1 = cursor.next();
                           System.out.println(dbo1);
                           Mongofilepath = string3;
                           FileWriter file = new FileWriter(Mongofilepath);
                           file.write(dbo1.toJson());
                           file.close();
                           System.out.println("Done");
                           MongoResult(dbo1);
                           spec = spec.body(file);
                     }
              } finally {
                     cursor.close();
              }
       }

       @Given("Header key {string} value {string}")
       public void header_key_value(String string, String string2) {
              spec = spec.header(string, string2);
       }

       @Given("Content type {string}")
       public void content_type(String string) {
              spec = spec.contentType(string);
       }

       @Given("Request body {string}")
       public void request_body(String string) {
              File f = new File(string);
              spec = spec.body(f);
       }
       @Given("Query param key {string} value {string}")
       public void query_param_key_value(String string, String string2) {
          spec = spec.queryParams(string, string2);
       }
       
       @Given("Run the EmailConFig")
       public static void run() {
    	   emailconfigtoJson.getWrite();

	}
       
//       public static void main(String[] args) {
//		run();
//	}


}

