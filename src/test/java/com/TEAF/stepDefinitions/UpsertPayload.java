package com.TEAF.stepDefinitions;


public class UpsertPayload {

private String Area_Manager__C;
private String Channel__C;
private String Closed_Date__C;
private String District_Manager__C;
private String Fax;
private String Name;
private String Open_Date__C;
private String RecordTypeId;
private String Warehouse_Code__C;
private String Warehouse_C;
private boolean Web_Store__C;
private String Regional_Vice_President__C;
private String Sales_Market__C;
private String ShippingCity;
private String ShippingCountry;
private String ShippingPostalCode;
private String ShippingState;
private String ShippingStreet;
private String Store_Region__C;
private String Store_WarehouseEmail__C;
private String Store_WarehouseStatusCodeC;
private String mFRMOracleRecordIDC;
private String phone;

public String getAreaManagerC() {
return Area_Manager__C;
}

public void setAreaManagerC(String Area_Manager__C) {
this.Area_Manager__C = Area_Manager__C;
}

public String getChannelC() {
return Channel__C;
}

public void setChannelC(String Channel__C) {
this.Channel__C = Channel__C;
}

public String getClosedDateC() {
return Closed_Date__C;
}

public void setClosedDateC(String Closed_Date__C) {
this.Closed_Date__C = Closed_Date__C;
}

public String getDistrictManagerC() {
return District_Manager__C;
}

public void setDistrictManagerC(String District_Manager__C) {
this.District_Manager__C = District_Manager__C;
}

public String getFax() {
return Fax;
}

public void setFax(String Fax) {
this.Fax = Fax;
}

public String getName() {
return Name;
}

public void setName(String Name) {
this.Name = Name;
}

public String getOpenDateC() {
return Open_Date__C;
}

public void setOpenDateC(String Open_Date__C) {
this.Open_Date__C = Open_Date__C;
}

public String getRecordTypeId() {
return RecordTypeId;
}

public void setRecordTypeId(String RecordTypeId) {
this.RecordTypeId = RecordTypeId;
}

public String getWarehouseCodeC() {
return Warehouse_Code__C;
}

public void setWarehouseCodeC(String Warehouse_Code__C) {
this.Warehouse_Code__C = Warehouse_Code__C;
}

public String getWarehouseC() {
return Warehouse_C;
}

public void setWarehouseC(String Warehouse_C) {
this.Warehouse_C = Warehouse_C;
}

public boolean isWebStoreC() {
return Web_Store__C;
}

public void setWebStoreC(boolean Web_Store__C) {
this.Web_Store__C = Web_Store__C;
}

public String getRegionalVicePresidentC() {
return Regional_Vice_President__C;
}

public void setRegionalVicePresidentC(String Regional_Vice_President__C) {
this.Regional_Vice_President__C = Regional_Vice_President__C;
}

public String getSalesMarketC() {
return Sales_Market__C;
}

public void setSalesMarketC(String Sales_Market__C) {
this.Sales_Market__C = Sales_Market__C;
}

public String getShippingCity() {
return ShippingCity;
}

public void setShippingCity(String ShippingCity) {
this.ShippingCity = ShippingCity;
}

public String getShippingCountry() {
return ShippingCountry;
}

public void setShippingCountry(String ShippingCountry) {
this.ShippingCountry = ShippingCountry;
}

public String getShippingPostalCode() {
return ShippingPostalCode;
}

public void setShippingPostalCode(String ShippingPostalCode) {
this.ShippingPostalCode = ShippingPostalCode;
}

public String getShippingState() {
return ShippingState;
}

public void setShippingState(String ShippingState) {
this.ShippingState = ShippingState;
}

public String getShippingStreet() {
return ShippingStreet;
}

public void setShippingStreet(String ShippingStreet) {
this.ShippingStreet = ShippingStreet;
}

public String getStoreRegionC() {
return Store_Region__C;
}

public void setStoreRegionC(String Store_Region__C) {
this.Store_Region__C = Store_Region__C;
}

public String getStoreWarehouseEmailC() {
return Store_WarehouseEmail__C;
}

public void setStoreWarehouseEmailC(String Store_WarehouseEmail__C) {
this.Store_WarehouseEmail__C = Store_WarehouseEmail__C;
}

public String getStoreWarehouseStatusCodeC() {
return Store_WarehouseStatusCodeC;
}

public void setStoreWarehouseStatusCodeC(String Store_WarehouseStatusCodeC) {
this.Store_WarehouseStatusCodeC = Store_WarehouseStatusCodeC;
}

public String getMFRMOracleRecordIDC() {
return mFRMOracleRecordIDC;
}

public void setMFRMOracleRecordIDC(String mFRMOracleRecordIDC) {
this.mFRMOracleRecordIDC = mFRMOracleRecordIDC;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

@Override
public String toString() {
	return "UpsertPayload [Area_Manager__C=" + Area_Manager__C + ", Channel__C=" + Channel__C + ", Closed_Date__C=" + Closed_Date__C
			+ ", District_Manager__C=" + District_Manager__C + ", Fax=" + Fax + ", Name=" + Name + ", Open_Date__C=" + Open_Date__C
			+ ", RecordTypeId=" + RecordTypeId + ", Warehouse_Code__C=" + Warehouse_Code__C + ", Warehouse_C=" + Warehouse_C
			+ ", Web_Store__C=" + Web_Store__C + ", Regional_Vice_President__C=" + Regional_Vice_President__C + ", Sales_Market__C="
			+ Sales_Market__C + ", ShippingCity=" + ShippingCity + ", ShippingCountry=" + ShippingCountry
			+ ", ShippingPostalCode=" + ShippingPostalCode + ", ShippingState=" + ShippingState + ", ShippingStreet="
			+ ShippingStreet + ", Store_Region__C=" + Store_Region__C + ", Store_WarehouseEmail__C=" + Store_WarehouseEmail__C
			+ ", Store_WarehouseStatusCodeC=" + Store_WarehouseStatusCodeC + ", mFRMOracleRecordIDC="
			+ mFRMOracleRecordIDC + ", phone=" + phone + "]";
}

}