package com.TEAF.stepDefinitions;

import java.util.ArrayList;
import java.util.List;

public class timestamp {

private List<Info> info = new ArrayList<Info>();

public List<Info> getInfo() {
return info;
}

public void setInfo(List<Info> info) {
this.info = info;
}

@Override
public String toString() {
	return "timestamp [info=" + info + "]";
}

}
