import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;

import com.TEAF.framework.GetPageObjectRead;

public class App {
	
	public static void i_enter_date_from_excelSheet(String value,String sheetname) throws Exception {
		try {
			java.io.File f = new java.io.File(System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			Date cellValue;	
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if(hr.getCell(i).getStringCellValue().equals(value)) {
				cellValue = content.getCell(i).getDateCellValue();
				SimpleDateFormat Dvalue = new SimpleDateFormat("dd-MM-yy");
				String DateValue = Dvalue.format(cellValue);
				System.out.println(DateValue.toString());
				} 
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		
		i_enter_date_from_excelSheet("DueDate", "AccountPayment");
		
	}

}
